Vue.prototype.apiUrl="http://127.0.0.1/"
Vue.prototype.$request=function(url,data=''){
	// var that=this
	return new Promise((reslove,reject)=>{
		uni.request({
			url:this.apiUrl+url,
			method:GET,
			header: {
				'token': uni.getStorageSync('token')
			},
			data:data,
			success:(res)=>{
				console.log(res)
				//token错误和返回结果都是用code
				if(res.data.tcode==0){
					reslove({"tcode":0,"msg":"未登录"})
					// this.code=0
				}
				if(res.data.tcode==1){
					reslove(res.data)
				}
				if(res.data.tcode==2){
					uni.setStorageSync('token',res.data.token)
					uni.request({
						url:this.apiUrl+url,
						method:"POST",
						header: {
							'token': uni.getStorageSync('token')
						},
						data:data,
						success:(res)=>{
							reslove(res.data)
						}
					})
				}
				if(res.data.tcode==402){
					reslove({"tcode":0,"msg":"token失效"})
					uni.setStorageSync("token",'')
				}
				if(res.data.tcode==4){
					reslove({"tcode":0,"msg":"没有相关数据"})
					uni.setStorageSync("token",'')
				}
				
				if(res.data.code==0||res.data.code){
					reslove(res.data)
				}
				
				
			}
		})
	})