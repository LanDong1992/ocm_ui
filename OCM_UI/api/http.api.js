
let categoryUrl = '/ocm-product/category'
let spuUrl = '/ocm-product/spu'
let productUrl='/ocm-product/'
let menuUrl='/ocm-app/menu'
let shopUrl='/ocm-shop/shop'
let userUrl='/ocm-user/'
let payUrl='/ocm-pay/'
let orderUrl='/ocm-order/'
// 此处第二个参数vm，就是我们在页面使用的this，你可以通过vm获取vuex等操作，更多内容详见uView对拦截器的介绍部分：
// https://uviewui.com/js/http.html#%E4%BD%95%E8%B0%93%E8%AF%B7%E6%B1%82%E6%8B%A6%E6%88%AA%EF%BC%9F
const install = (Vue, vm) => {
	//订单相关
	let createOrder=(params={})=>vm.$u.post(orderUrl+'order/u/create',params)
	let orderList=(params={})=>vm.$u.get(orderUrl+'order/u/list',params)
	let payOrder=(params={})=>vm.$u.post(orderUrl+'order/u/pay',params)
	let refundOrder=(params={})=>vm.$u.post(orderUrl+'order/u/refund',params)
	let orderDetail=(params={})=>vm.$u.get(orderUrl+'order/u/detail',params)
	let updateOrder=(params={})=>vm.$u.post(orderUrl+'order/u/update',params)
	let cancelOrder=(params={})=>vm.$u.post(orderUrl+'order/u/update',params)
	let receivingOrder=(params={})=>vm.$u.post(orderUrl+'order/u/receiving',params)
	let deleteOrder=(params={})=>vm.$u.post(orderUrl+'order/u/remove',params)
	//售后相关
	let afterSubmit=(params={})=>vm.$u.post(orderUrl+'orderafter/submit',params)
	let afterDetail=(params={})=>vm.$u.get(orderUrl+'orderafter/detail',params)
	let afterSend=(params={})=>vm.$u.post(orderUrl+'orderafter/send',params)
	let afterCancel=(params={})=>vm.$u.post(orderUrl+'orderafter/cancel',params)
	//商家相关
	let shopDetail=(id)=>vm.$u.get(shopUrl+"/detail?id="+id);
	//商品相关
	let searchProduct=(params={})=>vm.$u.get(spuUrl+"/search",params);
	let productDetail=(id)=>vm.$u.get(spuUrl+"/detail?spuId="+id);
	let skuDetail=(id)=>vm.$u.get('/ocm-product/sku/detail?spuId='+id);
	
	//购物车相关
	let addCart=(params={})=>vm.$u.post(productUrl+'cart',params)
	let getCart=()=>vm.$u.get(productUrl+'cart')
	let updateCart=(params={})=>vm.$u.post(productUrl+'cart/update',params)
	let delCart=(params={})=>vm.$u.post(productUrl+'cart/delete',params)
	let countCart=()=>vm.$u.get(productUrl+'cart/count')
	//分类相关
	let	categoryList =() => vm.$u.get(categoryUrl+"/list");
	
	//菜单相关
	let menuTree= (params = {}) => vm.$u.get(menuUrl+"/tree", params);
	let swiperList=(params={})=>vm.$u.get("/ocm-app"+"/swiper/list", params);
	//验证码相关
	let getCaptcha = () => vm.$u.get("/ocm-auth/captcha", null);
	let getPhoneCaptcha = (params={}) => vm.$u.get("/ocm-auth/phoneCaptcha", params);

	//支付相关
	let getWallet = () => vm.$u.get(payUrl+'userpayinfo/u/wallet');
	
	let withdraw=(params={})=>vm.$u.post(payUrl+'userpayinfo//u/withdraw',params)
	
	let certification=(params={})=>vm.$u.post(payUrl+'userpayinfo/u/certification',params)
	
	let getCertification=()=>vm.$u.get(payUrl+'userpayinfo/u/certification')
	
	let addBankCard=(params={})=>vm.$u.post(payUrl+'bankcard/u/bankcard',params);
	
	let getBankCardList=()=>vm.$u.get(payUrl+'bankcard/u/bankcard');
	
	let recharge=(params={})=>vm.$u.post(payUrl+'userpayinfo/u/recharge',params);
	
	let payList=(params={})=>vm.$u.get(payUrl+'paylist/u/page',params);
	
	//用户相关
	let userAddress=()=>vm.$u.get(userUrl+'address/u/list')
	let submitAddress=(params={})=>vm.$u.post(userUrl+'address/u/submit',params)
	let userDetail=(params={})=>vm.$u.get(userUrl+'user/u/detail',params)
	let updateUser=(params={})=>vm.$u.post(userUrl+'user/u/update',params)
	let updatePassword=(params={})=>vm.$u.post(userUrl+'user/updatePassword',params)
	let updatePhone=(params={})=>vm.$u.post(userUrl+'user/updatePhone',params)
	let getPhone = ()=>vm.$u.get(userUrl+"user/u/getPhone")
	let userPhoneCaptcha=(params={})=>vm.$u.get(userUrl+"user/phoneCaptcha",params)
	
	//用户登录
	let loginByUsername =  (params={}) => vm.$u.post('/ocm-auth/token'+
	    '?grantType=captcha'+
		'&account='+params.username+
		'&password='+params.password+
		'&tenantId='+params.tenantId
	  ,{},{
	    'Captcha-Key': params.key,
	    'Captcha-Code': params.code,
	 });
	 //用户注册
	 let userRegister =  (params={}) => vm.$u.post('/ocm-auth/register',params,{
	    'Captcha-Key': params.key,
	    'Captcha-Code': params.code,
	 });
	
	let	userLogin =(params = {}) => vm.$u.post(loginUrl, params);

	let	getUserInfo =(params = {}) => vm.$u.get(getUserInfoUrl, params);
	
	let	getProduct =(params = {}) => vm.$u.get(getProductUrl+"/"+params.id, {});
	
	let	getCategory =(params = {})=> vm.$u.get(getCategoryUrl,{});
	
	vm.$u.api = {
		createOrder,orderList,payOrder,refundOrder,orderDetail,cancelOrder,updateOrder,
			receivingOrder,deleteOrder,
		afterSubmit,afterDetail,afterSend,afterCancel,
		getWallet,certification,getBankCardList,addBankCard,getCertification,
			recharge,withdraw,payList,
		addCart,getCart,updateCart,delCart,countCart,
		shopDetail,
		categoryList,
		userAddress,submitAddress,updateUser,userDetail,getPhone,userPhoneCaptcha,
		updatePassword,updatePhone,
		menuTree,swiperList,
		getCaptcha,
		loginByUsername,
		searchProduct,productDetail,skuDetail,
		userRegister
		,getPhoneCaptcha
		};
}

export default {
	install
}