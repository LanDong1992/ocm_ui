// 这里的vm，就是我们在vue文件里面的this，所以我们能在这里获取vuex的变量，比如存放在里面的token
// 同时，我们也可以在此使用getApp().globalData，如果你把token放在getApp().globalData的话，也是可以使用的
const install = (Vue, vm) => {
	Vue.prototype.$u.http.setConfig({
			baseUrl: 'http://127.0.0.1:8000',
			// /method: 'GET', //POST/GET
			// 设置为json，返回后会对数据进行一次JSON.parse()
			dataType: 'json',
			showLoading: true, // 是否显示请求中的loading
			loadingText: '努力加载中~', // 请求loading中的文字提示
			loadingTime: 800, // 在此时间内，请求还没回来的话，就显示加载中动画，单位ms
			originalData: true, // 是否在拦截器中返回服务端的原始数据
			loadingMask: true, // 展示loading的时候，是否给一个透明的蒙层，防止触摸穿透
			// 配置请求头信息
			header: {
				'content-type': 'application/json;charset=utf-8'
			}
		})
	// 请求拦截，配置Token等参数
	Vue.prototype.$u.http.interceptor.request = (config) => {
		
		config.header = Object.assign(config.header, {
			'Authorization':'Basic b2NtQXBwOmFwcF9zZWNyZXQ=',
			'Blade-Auth':uni.getStorageSync('token')
		})
	
		// 方式一，存放在vuex的token，假设使用了uView封装的vuex方式，见：https://uviewui.com/components/globalVariable.html
		// config.header.token = vm.token;
		
		// 方式二，如果没有使用uView封装的vuex方法，那么需要使用$store.state获取
		// config.header.token = vm.$store.state.token;
		
		// 方式三，如果token放在了globalData，通过getApp().globalData获取
		// config.header.token = getApp().globalData.username;
		
		// 方式四，如果token放在了Storage本地存储中，拦截是每次请求都执行的，所以哪怕您重新登录修改了Storage，下一次的请求将会是最新值
		// const token = uni.getStorageSync('token');
		// config.header.token = token;
		
		return config; 
	}
	// 响应拦截，判断状态码是否通过
	Vue.prototype.$u.http.interceptor.response = (res) => {
		if(res.errMsg=="request:fail ")
			return vm.$u.toast("网络不可用");;
		res=res.data;
		
		// 如果把originalData设置为了true，这里得到将会是服务器返回的所有的原始数据
		// 判断可能变成了res.statueCode，或者res.data.code之类的，请打印查看结果
		
		switch (res.code){
			case 200:{
				return res.data; 
			}
				break;
			case 400:vm.$u.toast(res.msg);
				break;
			case 401:{
				uni.navigateTo({url:'/pages/login/login'})
				return null;
			};break;
			case 404:
				vm.$u.toast("服务暂不可用，请稍后重试");
			break;
			case 500:{
				vm.$u.toast(res.msg);
			};break;
			case 1001:{
				vm.$u.toast("根据相关法律规定,请先实名认证");
				uni.redirectTo({url:'/pages/certification/certification'})
			};break;
			default:vm.$u.toast(res.msg);
				break;
		}
	return false;
	}
}

export default {
	install
}