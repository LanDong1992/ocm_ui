import Vue from 'vue'
import Vuex from 'vuex'

import index from '@/store/index/index.js'
Vue.use(Vuex)

const store = new Vuex.Store({

	state: {
		ocmTheme: 'white',
		user:{},
		category:{},
		indexGridMenu:[],
	},
	
	mutations: {
		setOcmTheme(state, val) {
			state.ocmTheme = val
		},
		setUser(state,val){
			state.user = val
		},
		setCategory(state,val){
			state.category = val
		},
		setIndexGridMenu(state,val){
			state.indexGridMenu = val
		},
		
	},
	getters: {
	    getUser: state => state.user,
		getCategory:state => state.category,
		
	}
})


export default store
