export const _bankTypeMap = new Map([
    ['BOC',"中国银行"],
    ['CEB',"光大银行"],
    ['ICBC',"工商银行"],
    ['CMB',"招商银行"],
    ['CMBC',"民生银行"],
    ['HXB',"华夏银行"],
    ['CCB',"建设银行"],
    ['BCM',"交通银行"],
	['SPDB',"浦发银行"],
	['ABC',"农业银行"],
	['PSBC',"邮政储蓄银行"],
	['ZFB',"支付宝"],
	['WX',"微信"],
	['OCM',"一点钱包"]
])
export const _cardTypeMap =new Map([
    ['DC',"储蓄卡"],
])
export const _cardStyleMap =  new Map([
    ['BOC',{background: 'linear-gradient(to right, #FF7065, #FD4754)'}],
    ['CEB',{background: 'linear-gradient(to right, #9574ff, #a222fd)'}],
    ['ICBC',{background: 'linear-gradient(to right, #FF6F64, #FE5762)'}],
    ['CMB',{background: 'linear-gradient(to right, #FF6F64, #FE5762)'}],
    ['CMBC',{background: 'linear-gradient(to right, #65ff84, #14fe37)'}],
    ['HXB',{background: 'linear-gradient(to right, #ffa1a1, #fe7376)'}],
    ['CCB',{background: 'linear-gradient(to right, #2C87D6, #2D69D0)'}],
    ['BCM',{background: 'linear-gradient(to right, #2C85D5, #2D66D1)'}],
	['SPDB',{background: 'linear-gradient(to right, #FEAD4B, #FF9225)'}],
	['ABC',{background: 'linear-gradient(to right, #92d545, #00985e)'}],
	['PSBC',{background: 'linear-gradient(to right, #01ADA3, #0291A9)'}]
])